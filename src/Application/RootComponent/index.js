import React from "react";
import "./index.css";

export default ({children}) =>
    <div className="main-container">
        <div className="content-container">
            {children}
        </div>
    </div>;

