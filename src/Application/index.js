import React from "react";
import {Router, IndexRoute, Route, browserHistory} from "react-router";
import Root from "./RootComponent/index";
import MainView from "../views/MainView";
import RegisterView from "../views/RegisterView";
import CarView from "../views/CarView";

export default () =>
    <Router history={browserHistory}>
        <Route path="/" component={Root}>
            <IndexRoute component={MainView}/>
            <Route path="/new" component={RegisterView}/>
            <Route path="/car/:id" component={CarView}/>
        </Route>
    </Router>;