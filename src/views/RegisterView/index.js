import React, {Component} from "react";
import {Link, browserHistory} from "react-router";
import In from "../../components/ControlledInput";
import Navbar from "../../components/Navbar";
import CancelButton from "../../components/CancelButton";
import RegisterButton from "../../components/RegisterButton";
import {createSession} from "../../services/SessionService";
import "./style.css";

export default class RegisterView extends Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.onFieldChange = this.onFieldChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    onFieldChange(e) {
        if (!e || !e.target) return;
        const [object, property] = e.target.name.split('.');

        this.setState({
            [object]: {
                ...this.state[object],
                [property]: e.target.value,
            }
        });
    }

    submitForm() {
        this.setState({
            registering: true
        }, () => createSession(this.state.carInfo, this.state.usedCarInfo)
            .then(createdSessionId => browserHistory.push("/car/" + createdSessionId)));
    }

    render() {
        return (
            <div>
                <div className="navbar-container">
                    <Navbar/>
                </div>
                <div className="container-fluid">
                    <div
                        className="col-xs-12 col-sm-9 col-md-10 col-md-offset-2 col-lg-10 col-lg-offset-2 register-view-content">
                        <div className="col-md-5 col-lg-5">
                            <In name="carInfo.registrationNumber" label="Registreringsnummer"
                                value={this.state.registrationNumber} onChange={this.onFieldChange}/>
                            <In name="carInfo.brand" label="Merke" value={this.state.brand}
                                onChange={this.onFieldChange}/>
                            <In name="carInfo.model" label="Modell" value={this.state.model}
                                onChange={this.onFieldChange}/>
                            <In name="carInfo.color" label="Farge" value={this.state.color}
                                onChange={this.onFieldChange}/>
                            <In name="carInfo.mileage" label="Kilometerstand" value={this.state.mileage}
                                onChange={this.onFieldChange}/>
                            <In name="carInfo.modelYear" label="Årsmodell" value={this.state.modelYear}
                                onChange={this.onFieldChange}/>
                        </div>

                        <div className="col-md-5 col-lg-5">
                            <In name="usedCarInfo.usedCarNumber" label="Bruktbilnummer" value={this.state.usedCarNumber}
                                onChange={this.onFieldChange}/>
                            <In name="usedCarInfo.location" label="Lokasjon" value={this.state.location}
                                onChange={this.onFieldChange}/>
                            <In name="usedCarInfo.bought" label="Kjøpt" value={this.state.bought}
                                onChange={this.onFieldChange}/>
                            <In name="usedCarInfo.owner" label="Eier" value={this.state.owner}
                                onChange={this.onFieldChange}/>
                            <In name="usedCarInfo.assessedByUser" label="Taksert av" value={this.state.assessedByUser}
                                onChange={this.onFieldChange}/>
                            <In name="usedCarInfo.departmentMtakst" label="Mtakst avdeling"
                                value={this.state.departmentMtakst} onChange={this.onFieldChange}/>
                        </div>

                        <div className="col-md-10  col-lg-10 register-buttons-div text-right">
                            <Link to="/">
                                <CancelButton />
                            </Link>
                            <RegisterButton onClick={this.submitForm} disabled={this.state.registering}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
