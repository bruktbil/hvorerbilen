import React, {Component} from "react";
import {
    getSessionById,
    setFinishedInProcess,
    setSold,
    setAcquisitionCar,
    updateWorkItem,
    deleteWorkItem
} from "../../services/SessionService";
import {
    completeActiveStep,
    setCurrentStep,
    addComment,
    addLabour,
    editCarInfo,
    editUsedCarInfo
} from "../../services/SessionStateService";
import {isCurrentStepCompleted} from "../../utilities/StepLabels";
import Step from "../../enums/Step";
import View from "./View";

export default class CarView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            session: {}
        };

        this.completeStepClick = this.completeStepClick.bind(this);
        this.loadSession = this.loadSession.bind(this);
        this.changeStep = this.changeStep.bind(this);
        this.addCommentClick = this.addCommentClick.bind(this);
        this.addLabourClick = this.addLabourClick.bind(this);
        this.editCarInfoClick = this.editCarInfoClick.bind(this);
        this.editUsedCarInfoClick = this.editUsedCarInfoClick.bind(this);
        this.toggleFinishedInProcessClick = this.toggleFinishedInProcessClick.bind(this);
        this.toggleSoldClick = this.toggleSoldClick.bind(this);
        this.toggleAcquisitionCarClick = this.toggleAcquisitionCarClick.bind(this);
        this.updateWorkItemClick = this.updateWorkItemClick.bind(this);
        this.deleteWorkItemClick = this.deleteWorkItemClick.bind(this);
    }

    loadSession() {
        getSessionById(this.props.params.id)
            .then(res => this.setState({session: res}));
    }

    componentDidMount() {
        this.loadSession();
    }

    completeStepClick() {
        completeActiveStep(this.state.session.id)
            .then(result => this.loadSession());
    }

    changeStep(stepName) {
        setCurrentStep(this.state.session.id, stepName)
            .then(result => this.loadSession());
    }

    addCommentClick(sessionStepEnum, commentText) {
        addComment(this.state.session.id, sessionStepEnum, commentText)
            .then(result => this.loadSession());
    }

    addLabourClick(labourObject) {
        addLabour(this.state.session.id, labourObject)
            .then(result => this.loadSession());
    }

    editCarInfoClick(carInfo) {
        editCarInfo(this.state.session.id, carInfo)
            .then(result => this.loadSession());
    }

    editUsedCarInfoClick(usedCarInfo) {
        editUsedCarInfo(this.state.session.id, usedCarInfo)
            .then(result => this.loadSession());
    }

    toggleFinishedInProcessClick() {
        setFinishedInProcess(this.state.session.id, !this.state.session.finishedInProcess)
            .then(result => this.loadSession());
    }

    toggleSoldClick() {
        setSold(this.state.session.id, !this.state.session.sold)
            .then(result => this.loadSession());
    }

    toggleAcquisitionCarClick() {
        setAcquisitionCar(this.state.session.id, !this.state.session.acquisitionCar)
            .then(result => this.loadSession());
    }

    updateWorkItemClick(workItemIndex, workItemObject) {
        updateWorkItem(this.state.session.id, workItemIndex, workItemObject)
            .then(result => this.loadSession());
    }

    deleteWorkItemClick(workItemIndex) {
        deleteWorkItem(this.state.session.id, workItemIndex)
            .then(result => this.loadSession());
    }

    render() {
        const state = Object.assign({}, this.state.session);
        const {carInfo, usedCarInfo, comments, labourOperations, logItems, states, currentStep, finishedInProcess, sold, acquisitionCar} = state;

        return <View
            carInfo={carInfo}
            usedCarInfo={usedCarInfo}
            comments={comments}
            labourOperations={labourOperations}
            logItems={logItems}
            stepStates={states}
            currentStepCompleted={isCurrentStepCompleted(states, currentStep)}
            completeStepClick={this.completeStepClick}
            changeStepClick={this.changeStep}
            currentStepName={Step[currentStep] && Step[currentStep].label}
            addCommentClick={this.addCommentClick}
            addLabourClick={this.addLabourClick}
            editCarInfoClick={this.editCarInfoClick}
            editUsedCarInfoClick={this.editUsedCarInfoClick}
            finishedInProcessClick={this.toggleFinishedInProcessClick}
            soldClick={this.toggleSoldClick}
            acquisitionCarClick={this.toggleAcquisitionCarClick}
            isFinishedInProcess={finishedInProcess}
            isSold={sold}
            isAcquisitionCar={acquisitionCar}
            startedInProcess={currentStep !== Step.NOT_STARTED.name}
            updateWorkItemClick={this.updateWorkItemClick}
            deleteWorkItemClick={this.deleteWorkItemClick}
        />;
    }
}