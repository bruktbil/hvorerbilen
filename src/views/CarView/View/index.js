import React from "react";
import Card from "../../../components/Card";
import CommentsCard from "../../../components/CommentsCard";
import WorkCard from "../../../components/WorkCard";
import SubNav from "../../../components/SubNav";
import Navbar from "../../../components/Navbar";
import Toolbar from "../../../components/Toolbar";
import SubNavMobile from "../../../components/SubNavMobile";
import ProgressBar from "../../../components/ProgressBar";
import CarStatus from "../../../components/CarStatus";
import AddComment from "../../../components/AddComment";
import LabourOperationModal from "../../../components/LabourOperationModal";
import AddButton from "../../../components/AddButton";
import EditIcon from "../../../components/EditIcon";
import EditCarInfo from "../../../components/EditCarInfo";
import EditUsedCarInfo from "../../../components/EditUsedCarInfo";
import Modal from "../../../components/Modal";
import CarInfo from "../../../components/CarInfo";
import UsedCarInfo from "../../../components/UsedCarInfo";
import LabourOperations from "../../../components/LabourOperations";
import LogItems from "../../../components/LogItems";
import Comments from "../../../components/Comments";
import "./style.css";

const editCarInfoModalId = "editCarInfoModal";
const editUsedCarInfoModalId = "editCarUsedInfoModal";
const addWorkModalId = "addWorkModal";
const addCommentModalId = "addCommentModalId";

export default ({
    carInfo,
    usedCarInfo,
    labourOperations,
    comments,
    logItems,
    stepStates,
    completeStepClick,
    currentStepCompleted,
    changeStepClick,
    currentStepName,
    addCommentClick,
    addLabourClick,
    editCarInfoClick,
    editUsedCarInfoClick,
    finishedInProcessClick,
    soldClick,
    acquisitionCarClick,
    isFinishedInProcess,
    isSold,
    isAcquisitionCar,
    startedInProcess,
    updateWorkItemClick,
    deleteWorkItemClick
}) =>
    <div>
        <div className="navbar-container hidden-xs">
            <Navbar/>
        </div>
        <div className="visible-xs">
            <Toolbar registrationNumber={carInfo && carInfo.registrationNumber}
                     isSold={isSold}
                     isAcquisitionCar={isAcquisitionCar}
                     finishedInProcessClick={finishedInProcessClick}
                     isFinishedInProcess={isFinishedInProcess}
                     soldClick={soldClick}
                     acquisitionCarClick={acquisitionCarClick}/>
        </div>
        <div className="view-container">
            <div className="col-xs-12 col-sm-9 col-md-9 main-view-content">
                <div className="row">
                    <div className="col-sm-12 no-gutters">
                        <SubNav registrationNumber={carInfo && carInfo.registrationNumber}
                                completeStepClick={completeStepClick}
                                currentStepCompleted={currentStepCompleted}
                                changeStepClick={changeStepClick}
                                currentStepName={currentStepName}
                                finishedInProcessClick={finishedInProcessClick}
                                soldClick={soldClick}
                                acquisitionCarClick={acquisitionCarClick}
                                isFinishedInProcess={isFinishedInProcess}
                                isSold={isSold}
                                isAcquisitionCar={isAcquisitionCar}
                                startedInProcess={startedInProcess}/>
                        <SubNavMobile completeStepClick={completeStepClick}
                                      currentStepCompleted={currentStepCompleted}
                                      changeStepClick={changeStepClick}
                                      isFinishedInProcess={isFinishedInProcess}
                                      startedInProcess={startedInProcess}
                                      currentStepName={currentStepName}/>
                    </div>
                </div>
                <div className="main-view-margin">
                    <div className="row">
                        <div className="col-xs-12">
                            <ProgressBar stepStates={stepStates}/>
                        </div>
                    </div>
                    <div className="col-xs-12">
                        <CarStatus />
                    </div>
                    <div className="row">
                        <div className="col-sm-12 col-md-4 grey-box-spacing">
                            <Card title="Bilinfo">
                                <EditIcon modalId={editCarInfoModalId} isFinishedInProcess={isFinishedInProcess}/>
                                <Modal title="Rediger bilinfo" modalId={editCarInfoModalId}>
                                    <EditCarInfo {...carInfo} editCarInfoClick={editCarInfoClick}/>
                                </Modal>
                                <CarInfo {...carInfo} />
                            </Card>
                        </div>
                        <div className="col-sm-12 col-md-4 grey-box-spacing">
                            <Card title="Bruktbilinfo">
                                <EditIcon modalId={editUsedCarInfoModalId} isFinishedInProcess={isFinishedInProcess}/>
                                <Modal title="Rediger bruktbilinfo" modalId={editUsedCarInfoModalId}>
                                    <EditUsedCarInfo {...usedCarInfo} editUsedCarInfoClick={editUsedCarInfoClick}/>
                                </Modal>
                                <UsedCarInfo {...usedCarInfo} />
                            </Card>
                        </div>
                        <div className="col-sm-12 col-md-4 grey-box-spacing">
                            <WorkCard title="Arbeid på bil">
                                <LabourOperations labourOperations={labourOperations}
                                                  isFinishedInProcess={isFinishedInProcess}
                                                  updateWorkItemClick={updateWorkItemClick}
                                                  deleteWorkItemClick={deleteWorkItemClick}/>
                                <AddButton modalId={addWorkModalId} isFinishedInProcess={isFinishedInProcess}/>
                                <Modal title="Legg til arbeid" modalId={addWorkModalId}>
                                    <LabourOperationModal persistLabourClick={addLabourClick}/>
                                </Modal>
                            </WorkCard>
                        </div>
                        <div className="col-sm-12 grey-box-spacing">
                            <CommentsCard title="Kommentarer">
                                <Comments comments={comments}/>
                                <AddButton modalId={addCommentModalId} isFinishedInProcess={isFinishedInProcess}/>
                                <Modal title="Legg til kommentar" modalId={addCommentModalId}>
                                    <AddComment addCommentClick={addCommentClick}/>
                                </Modal>
                            </CommentsCard>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-xs-12 col-sm-3 col-md-3 log-container no-gutters hidden-xs">
                <LogItems logItems={logItems}/>
            </div>
        </div>
    </div>;
