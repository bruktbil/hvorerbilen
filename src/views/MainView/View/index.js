import React from "react";
import Navbar from "../../../components/Navbar";
import SessionRow from "../../../components/SessionRow";
import TabsMenuMobile from "../../../components/TabsMenuMobile";
import Step from "../../../enums/Step";
import "./style.css";

export default ({sessions = []}) => {
    const startedSessions = sessions
        .filter(session => session.currentStep !== Step.NOT_STARTED.name && session.acquisitionCar === false);
    const notStartedSessions = sessions
        .filter(session => session.currentStep === Step.NOT_STARTED.name && session.acquisitionCar === false);
    const acquisitionCarSessions = sessions.filter(session => session.acquisitionCar === true);

    return (
        <div>
            <div className="navbar-container">
                <Navbar/>
            </div>
            <div className="view-container">
                <TabsMenuMobile startedSessions={startedSessions}
                                notStartedSessions={notStartedSessions}
                                acquisitionCarSessions={acquisitionCarSessions}/>
                <div className="col-xs-12 col-sm-12 col-md-12 list-content hidden-xs">
                    <div className="col-md-4 col-sm-12 list-box-container">
                        <div className="list-box">
                            <div className="list-title-box">
                                <h4 className="list-title">Ikke startet i prosess</h4>
                                <h4 className="list-number">{notStartedSessions.length}</h4>
                            </div>
                            <div className="list-container">
                                <table className="table table-striped table-hover">
                                    <thead>
                                    <tr className="list-head">
                                        <td className="list-head-item"><b>Regnr</b></td>
                                        <td className="list-head-item"><b>Modell</b></td>
                                        <td className="list-head-item"><b>Opprettet</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {notStartedSessions.map((session, index) =>
                                        <SessionRow
                                            key={index}
                                            isSold={session.sold}
                                            url={"/car/" + session.id}
                                            registrationNumber={session.carInfo.registrationNumber}
                                            model={session.carInfo.model}
                                            createdAt={session.usedCarInfo.bought}/>
                                    )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-sm-12 list-box-container">
                        <div className="list-box">
                            <div className="list-title-box-started">
                                <h4 className="list-title">Startet i prosess</h4>
                                <h4 className="list-number">{startedSessions.length}</h4>
                            </div>
                            <div className="list-container">
                                <table className="table table-striped table-hover">
                                    <thead>
                                    <tr className="list-head">
                                        <td className="list-head-item"><b>Regnr</b></td>
                                        <td className="list-head-item"><b>Steg</b></td>
                                        <td className="list-head-item"><b>Modell</b></td>
                                        <td className="list-head-item"><b>Opprettet</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {startedSessions.map((session, index) =>
                                        <SessionRow
                                            key={index}
                                            url={"/car/" + session.id}
                                            registrationNumber={session.carInfo.registrationNumber}
                                            location={Step[session.currentStep].label}
                                            model={session.carInfo.model}
                                            isSold={session.sold}
                                            createdAt={session.usedCarInfo.bought}/>)}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 col-sm-12 list-box-container">
                        <div className="list-box">
                            <div className="list-title-box">
                                <h4 className="list-title">Oppkjøperbil</h4>
                                <h4 className="list-number">{acquisitionCarSessions.length}</h4>
                            </div>
                            <div className="list-container">
                                <table className="table table-striped table-hover">
                                    <thead>
                                    <tr className="list-head">
                                        <td className="list-head-item"><b>Regnr</b></td>
                                        <td className="list-head-item"><b>Modell</b></td>
                                        <td className="list-head-item"><b>Opprettet</b></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {acquisitionCarSessions.map((session, index) =>
                                        <SessionRow
                                            key={index}
                                            url={"/car/" + session.id}
                                            registrationNumber={session.carInfo.registrationNumber}
                                            model={session.carInfo.model}
                                            isSold={session.sold}
                                            createdAt={session.usedCarInfo.bought}/>)}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
