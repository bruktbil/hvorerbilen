import React, {Component} from "react";
import {getAllSessions} from "../../services/SessionService";
import View from "./View";

export default class MainView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sessions: []
        };
    }

    componentDidMount() {
        getAllSessions()
            .then(res => this.setState({sessions: res}));
    }

    render() {
        return <View sessions={this.state.sessions} />;
    }
}