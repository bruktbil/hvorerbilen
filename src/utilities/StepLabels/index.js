import ProgressBarStatus from "../../enums/ProgressBarStatus";
import Step from "../../enums/Step";

const inProgressStepState = "IN_PROGRESS";
const finishedStepState = "FINISHED";
const notStartedState = "NOT_STARTED";

export const getStateForStep = stepState => {
    switch (stepState) {
        case inProgressStepState:
            return ProgressBarStatus.STATUS_CURRENT;
        case finishedStepState:
            return ProgressBarStatus.STATUS_DONE;
        case notStartedState:
        default:
            return ProgressBarStatus.STATUS_DEFAULT;
    }
};

export const isStepActive = step => step === inProgressStepState;

export const isCurrentStepCompleted = (states = {}, currentStep) => {
    if (!currentStep) return false;
    const currentStepFieldName = Step[currentStep].fieldName;
    return states[currentStepFieldName] === finishedStepState;
};