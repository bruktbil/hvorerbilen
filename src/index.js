import React from "react";
import ReactDOM from "react-dom";
import Application from "./Application/index";
import "./Application/RootComponent/index.css";

const rootElementById = document.getElementById('root');
ReactDOM.render(<Application />, rootElementById);

if (module.hot) {
    module.hot.accept('./Application', () => {
        const NextApplication = require('./Application').default;
        ReactDOM.render(<NextApplication />, rootElementById)
    })
}