import React, {Component} from "react";
import In from "../ControlledInput";
import CancelButton from "../../components/CancelButton";
import SaveButton from "../../components/SaveButton";

export default class EditUsedCarInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.onFieldChange = this.onFieldChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.usedCarNumber) {
            const {usedCarNumber, location, bought, owner, departmentMtakst} = nextProps;
            this.setState({usedCarNumber, location, bought, owner, departmentMtakst});
        }
    }

    onFieldChange(e) {
        if (e && e.target) {
            this.setState({
                ...this.state,
                [e.target.name]: e.target.value,
            });
        }
    }

    render() {
        if (!this.state.usedCarNumber) {
            return <div className="container-fluid modal-body">Feil: kunne ikke få data fra rotkomponenten.</div>;
        }

        const {usedCarNumber, location, bought, owner, departmentMtakst} = this.state;
        return (
            <div className="container-fluid">
                <div className="modal-body col-md-10">
                    <In onChange={this.onFieldChange} label="Bruktbilnr" value={usedCarNumber} name="usedCarNumber"/>
                    <In onChange={this.onFieldChange} label="Lokasjon" value={location} name="location"/>
                    <In onChange={this.onFieldChange} label="Kjøpt" value={bought} name="bought"/>
                    <In onChange={this.onFieldChange} label="Eier" value={owner} name="owner"/>
                    <In onChange={this.onFieldChange} label="Taksert av" value={departmentMtakst}
                        name="departmentMtakst"/>
                </div>
                <div className="modal-footer">
                    <CancelButton />
                    <SaveButton onClick={() => this.props.editUsedCarInfoClick(this.state)}/>
                </div>
            </div>
        );
    }
}