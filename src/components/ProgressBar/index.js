import React from "react";
import ProgressBarElement from "./ProgressBarElement";
import ProgressBarElementText from "./ProgressBarElementText";
import {isStepActive, getStateForStep} from "../../utilities/StepLabels";
import Step from "../../enums/Step";
import "./style.css";

export default ({
    stepStates: {
        valuation,
        reception,
        repairsWorkshop,
        technicalWorkshop,
        washAndShine,
        photo,
        publishing,
    } = {}
}) =>
    <div className="panel-z-index-horizontal progress-bar-container hidden-xs">
        <div className="progress-bar-element-text-container">
            <ProgressBarElementText text={Step.VALUATION.label} current={isStepActive(valuation)}/>
            <ProgressBarElementText text={Step.RECEPTION.label} current={isStepActive(reception)}/>
            <ProgressBarElementText text={Step.REPAIRS_WORKSHOP.label} current={isStepActive(repairsWorkshop)}/>
            <ProgressBarElementText text={Step.TECHNICAL_WORKSHOP.label} current={isStepActive(technicalWorkshop)}/>
            <ProgressBarElementText text={Step.WASH_AND_SHINE.label} current={isStepActive(washAndShine)}/>
            <ProgressBarElementText text={Step.PHOTO.label} current={isStepActive(photo)}/>
            <ProgressBarElementText text={Step.PUBLISHING.label} current={isStepActive(publishing)}/>
        </div>
        <div className="progress-bar-elements">
            <ProgressBarElement status={getStateForStep(valuation)}/>
            <ProgressBarElement status={getStateForStep(reception)}/>
            <ProgressBarElement status={getStateForStep(repairsWorkshop)}/>
            <ProgressBarElement status={getStateForStep(technicalWorkshop)}/>
            <ProgressBarElement status={getStateForStep(washAndShine)}/>
            <ProgressBarElement status={getStateForStep(photo)}/>
            <ProgressBarElement status={getStateForStep(publishing)}/>
        </div>
    </div>;
