import React from "react";
import "./style.css";

export default ({text, current}) =>
    <div className="vertical-center-text">
        <small className={current ? "progress-bar-element-text-current" : "hidden-xs"}>{text}</small>
    </div>;