import React from "react";
import ProgressBarStatus from "../../../enums/ProgressBarStatus";
import "./style.css";

export default ({title, status = ProgressBarStatus.STATUS_DEFAULT}) =>
    <div className="seven-col">
        <div className={"progress-bar-element-horizontal " + status}></div>
    </div>;
