import React from "react";
import {browserHistory} from "react-router";
import "./style.css";

const linkCar = (url) => browserHistory.push(url);

export default ({url, registrationNumber, location, model, createdAt, isSold}) => {
    const showSoldIcon = isSold ? "icon ion-checkmark-circled sold-icon" : "";

    return (
        <tr onClick={() => linkCar(url)} className="list-row">
            {url ?
                <td>
                    {registrationNumber}
                    &nbsp;
                    <i className={showSoldIcon} />
                </td> :
                <td>
                    {registrationNumber}
                </td>}
            {location && <td>{location}</td>}
            <td>{model}</td>
            <td>{createdAt}</td>
        </tr>
    );
};