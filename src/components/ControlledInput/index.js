import React from "react";

export default ({label, value, onChange, name, onBlur}) =>
    <div className="form-group label-floating">
        <label className="control-label" htmlFor="registrationNumber">
            {label}
        </label>
        <input
            value={value}
            onChange={onChange}
            className="form-control"
            onBlur={onBlur}
            id={label}
            name={name}
            type="text"/>
    </div>;
