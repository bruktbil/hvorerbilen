import React from "react";

export default ({showItem, cancel}) =>
    <div className={"visible-xs container row" + (showItem === -1 ? " hidden-xs" : "")}>
        <div className="col-sm-12 text-left">
            <span style={{marginTop: 20, cursor: "pointer"}} className="glyphicon glyphicon-remove" onClick={cancel}/>
        </div>
    </div>;