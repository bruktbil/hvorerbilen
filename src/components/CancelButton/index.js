import React from "react";
import "./style.css";

export default () =>
    <div className="btn cancel-button" data-dismiss="modal">
        Avbryt
        <i className="icon ion-close-round icon-plus"/>
    </div>;
