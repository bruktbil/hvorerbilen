import React from "react";
import ConfirmButton from "../../components/ConfirmButton";
import CancelButton from "../../components/CancelButton";

export default ({deleteWorkItemClick}) =>
    <div className="container-fluid">
        <div className="modal-body"></div>
        <div className="modal-footer">
            <CancelButton />
            <ConfirmButton onClick={deleteWorkItemClick}/>
        </div>
    </div>;