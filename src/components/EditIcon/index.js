import React from "react";
import "./style.css";

export default ({modalId, isFinishedInProcess}) => {
    const disabledIcon = isFinishedInProcess ? "disabled" : "false";
    const disabledIconModal = isFinishedInProcess ? "" : "modal";
    const disabledIconCursor = isFinishedInProcess ? "disabled-cursor" : "";

    return (
        <div className="edit-icon-box">
            <a href={"#" + modalId} data-toggle={disabledIconModal} disabled={disabledIcon}
               className={disabledIconCursor}>
                <i className="icon ion-edit icon-edit"/>
            </a>
        </div>)
};
