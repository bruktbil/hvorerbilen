import React from "react";
import "./style.css";

export default ({usedCarNumber, owner, bought, location, assessedByUser, departmentMtakst}) =>
    <div className="container-fluid">
        <div className="row info-item">
            <div className="col-xs-6 info-item-label">Bruktbilnr.</div>
            <div className="col-xs-6 text-right info-item-text">{usedCarNumber}</div>
        </div>
        <div className="row info-item">
            <div className="col-xs-6 info-item-label">Lokasjon</div>
            <div className="col-xs-6 text-right info-item-text">{location}</div>
        </div>
        <div className="row info-item">
            <div className="col-xs-6 info-item-label">Kjøpt</div>
            <div className="col-xs-6 text-right info-item-text">{bought}</div>
        </div>
        <div className="row info-item">
            <div className="col-xs-6 info-item-label">Eier</div>
            <div className="col-xs-6 text-right info-item-text">{owner}</div>
        </div>
        <div className="row info-item">
            <div className="col-xs-6 info-item-label">Taksert av</div>
            <div className="col-xs-6 text-right info-item-text">{assessedByUser}</div>
        </div>
        <div className="row info-item-last">
            <div className="col-xs-6 info-item-label">Mtakst avd.</div>
            <div className="col-xs-6 text-right info-item-text">{departmentMtakst}</div>
        </div>
    </div>;
