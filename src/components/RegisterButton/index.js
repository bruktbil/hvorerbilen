import React from "react";
import PrimaryButton from "../PrimaryButton";

export default ({onClick, isFinishedInProcess, disabled}) =>

    <PrimaryButton buttonText="Registrer"
                   iconClass="ion-plus-round"
                   onClick={onClick}
                   disabled={disabled ? "disabled" : ""}/>;