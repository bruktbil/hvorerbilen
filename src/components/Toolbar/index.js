import React, {Component} from "react";
import DrawerMenu from "../../components/DrawerMenu";
import "./style.css";

export default class openNav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: 0
        };

        this.toggleOpen = this.toggleOpen.bind(this);
    };

    toggleOpen() {
        this.setState({
            width: this.state.width === 250 ? 0 : 250
        });
    }

    render() {
        let registrationNumberSuffix = "";
        if (this.props.isSold) {
            registrationNumberSuffix = "Solgt";
        } else if (this.props.isAcquisitionCar) {
            registrationNumberSuffix = "Oppkjøperbil";
        }

        return (
            <div className="nav-container">
                <div className="navbar navbar-inverse custom-nav">
                    <div className="container-fluid toolbar-padding">
                        <span className="hamburger-icon col-xs-1" onClick={this.toggleOpen}>
                            <i className="icon ion-navicon"/>
                        </span>
                        <DrawerMenu style={{width: this.state.width}} onClose={this.toggleOpen}/>
                        <a className="title-car-mobile col-xs-5">
                            {this.props.registrationNumber}
                        </a>

                        <div className="more-circle col-xs-1 col-sm-3 pull-right">
                            <a className="dropdown-toggle" data-toggle="dropdown">
                                <i className="icon ion-android-more-vertical more-icon"/>
                            </a>
                            <ul className="dropdown-menu more-dropdown-mobile">
                                <li onClick={this.props.finishedInProcessClick}>Ferdig i
                                    prosess {this.props.isFinishedInProcess && "✔"}</li>
                                <li onClick={this.props.soldClick}>Solgt {this.props.isSold && "✔"}</li>
                                <li onClick={this.props.acquisitionCarClick}>
                                    Oppkjøperbil {this.props.isAcquisitionCar && "✔"}</li>
                            </ul>
                        </div>

                        <a className="label-car-mobile pull-right col-xs-3 col-sm-2">
                            {registrationNumberSuffix}
                        </a>

                    </div>
                </div>
            </div>
        );
    }
}