import React from "react";
import "./style.css";
import Step, {getStepNameFromStepLabel} from "../../enums/Step";


export default ({
    completeStepClick,
    currentStepCompleted,
    changeStepClick,
    isFinishedInProcess,
    startedInProcess,
    currentStepName
}) => {
    const stepStatusStyle = {color: isFinishedInProcess ? "gray" : currentStepCompleted ? "green" : "white"};
    const chooseStepDisabledText = {color: currentStepCompleted && !isFinishedInProcess ? "rgba(0, 0, 0, 0.87)" : "#999"};
    const chooseStepDisabledToggle = currentStepCompleted && !isFinishedInProcess ? "" : "disabled";
    const finishStepDisabled = {backgroundColor: !isFinishedInProcess ? "#005c97" : "#999"};
    const disabledButtonClass = isFinishedInProcess ? "btn disabled-button-mobile" : "btn add-button";
    const stepStateText =
        startedInProcess ?
            (currentStepCompleted ? "Steg fullført" : "Fullfør steg") :
            (currentStepCompleted ? "Prosess startet" : "Start prosess");

    const stepName = getStepNameFromStepLabel(Step, currentStepName);

    return (
        <div className="visible-xs visible-sm row sub-nav-mobile">
            <div className="form-group col-xs-6 select-step">
                <label className="control-label">
                    <span>Velg steg</span>
                </label><br/>
                <select onChange={e => changeStepClick(e.target.value)} id="step" className="form-control"
                        disabled={chooseStepDisabledToggle} style={chooseStepDisabledText} value={stepName}>
                    <option default="default" disabled="disabled">Ingen valgt</option>
                    <option value={Step.VALUATION.name}>{Step.VALUATION.label}</option>
                    <option value={Step.RECEPTION.name}>{Step.RECEPTION.label}</option>
                    <option value={Step.REPAIRS_WORKSHOP.name}>{Step.REPAIRS_WORKSHOP.label}</option>
                    <option value={Step.TECHNICAL_WORKSHOP.name}>{Step.TECHNICAL_WORKSHOP.label}</option>
                    <option value={Step.WASH_AND_SHINE.name}>{Step.WASH_AND_SHINE.label}</option>
                    <option value={Step.PHOTO.name}>{Step.PHOTO.label}</option>
                    <option value={Step.PUBLISHING.name}>{Step.PUBLISHING.label}</option>
                </select>
            </div>
            <div className="finish-step-button col-xs-6 text-right">
                <a href="#" onClick={!isFinishedInProcess && completeStepClick}>
                    <div className={disabledButtonClass} style={finishStepDisabled}>
                        {stepStateText}
                        <i className="icon ion-checkmark-round icon-plus" style={stepStatusStyle}/>
                    </div>
                </a>
            </div>
        </div>
    )
};



