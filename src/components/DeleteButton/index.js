import React from "react";
import "./style.css";

export default ({modalId}) =>
    <a href={"#" + modalId} data-toggle="modal">
        <div className="btn delete-button">
            <i className="icon ion-trash-a trash-icon"/>
            Slett
        </div>
    </a>;
