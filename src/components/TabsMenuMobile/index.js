import React from "react";
import SessionRow from "../../components/SessionRow";
import Step from "../../enums/Step";
import "./style.css";

export default ({startedSessions, notStartedSessions, acquisitionCarSessions}) =>
    <div className="visible-xs no-border">
        <ul className="nav nav-tabs">
            <li className="col-xs-4 active" href="#ikke-startet" data-toggle="tab"><a>Ikke startet <span className="badge">{notStartedSessions.length}</span></a></li>
            <li className="col-xs-4" href="#startet" data-toggle="tab"><a>Startet <span className="badge">{startedSessions.length}</span></a></li>
            <li className="col-xs-4" href="#oppkjoper" data-toggle="tab"><a>Oppkjøper <span className="badge">{acquisitionCarSessions.length}</span></a></li>
        </ul>
        <div id="myTabContent" className="tab-content">
            <div className="tab-pane fade active in" id="ikke-startet">
                <div className="col-xs-12 list-box-container">
                    <div className="list-box-mobile">
                        <div className="list-container">
                            <table className="table table-striped table-hover">
                                <thead>
                                <tr className="list-head">
                                    <td className="list-head-item"><b>Regnr</b></td>
                                    <td className="list-head-item"><b>Modell</b></td>
                                    <td className="list-head-item"><b>Opprettet</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                {notStartedSessions.map((session, index) =>
                                    <SessionRow
                                        key={index}
                                        url={"/car/" + session.id}
                                        registrationNumber={session.carInfo.registrationNumber}
                                        model={session.carInfo.model}
                                        isSold={session.sold}
                                        createdAt={session.usedCarInfo.bought}/>)}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div className="tab-pane fade" id="startet">
                <div className="col-md-4 col-sm-12 list-box-container">
                    <div className="list-box-mobile">
                        <div className="list-container">
                            <table className="table table-striped table-hover">
                                <thead>
                                <tr className="list-head">
                                    <td className="list-head-item"><b>Regnr</b></td>
                                    <td className="list-head-item"><b>Steg</b></td>
                                    <td className="list-head-item"><b>Modell</b></td>
                                    <td className="list-head-item"><b>Opprettet</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                {startedSessions.map((session, index) =>
                                    <SessionRow
                                        key={index}
                                        url={"/car/" + session.id}
                                        registrationNumber={session.carInfo.registrationNumber}
                                        location={Step[session.currentStep].label}
                                        model={session.carInfo.model}
                                        isSold={session.sold}
                                        createdAt={session.usedCarInfo.bought}/>)}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div className="tab-pane fade" id="oppkjoper">
                <div className="col-md-4 col-sm-12 list-box-container">
                    <div className="list-box-mobile">
                        <div className="list-container">
                            <table className="table table-striped table-hover">
                                <thead>
                                <tr className="list-head">
                                    <td className="list-head-item"><b>Regnr</b></td>
                                    <td className="list-head-item"><b>Modell</b></td>
                                    <td className="list-head-item"><b>Opprettet</b></td>
                                </tr>
                                </thead>
                                <tbody>
                                {acquisitionCarSessions.map((session, index) =>
                                    <SessionRow
                                        key={index}
                                        url={"/car/" + session.id}
                                        registrationNumber={session.carInfo.registrationNumber}
                                        model={session.carInfo.model}
                                        isSold={session.sold}
                                        createdAt={session.usedCarInfo.bought}/>)}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>;
