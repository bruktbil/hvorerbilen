import React from "react";
import "./style.css";

export default ({title, children, modalId}) =>
    <div className="modal" id={modalId}>
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 className="modal-title">{title}</h4>
                </div>
                <div>{children}</div>
            </div>
        </div>
    </div>;