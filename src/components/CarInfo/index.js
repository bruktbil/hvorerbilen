import React from "react";
import "./style.css";

export default ({registrationNumber, brand, model, color, mileage, modelYear}) =>
    <div className="container-fluid">
        <div className="row info-item">
            <div className="col-xs-6 info-item-label">Registreringsnr.</div>
            <div className="col-xs-6 text-right info-item-text">{registrationNumber}</div>
        </div>
        <div className="row info-item">
            <div className="col-xs-6 info-item-label">Merke</div>
            <div className="col-xs-6 text-right info-item-text">{brand}</div>
        </div>
        <div className="row info-item">
            <div className="col-xs-6 info-item-label">Modell</div>
            <div className="col-xs-6 text-right info-item-text">{model}</div>
        </div>
        <div className="row info-item">
            <div className="col-xs-6 info-item-label">Farge</div>
            <div className="col-xs-6 text-right info-item-text">{color}</div>
        </div>
        <div className="row info-item">
            <div className="col-xs-6 info-item-label">Kilometerstand</div>
            <div className="col-xs-6 text-right info-item-text">{mileage}</div>
        </div>
        <div className="row info-item-last">
            <div className="col-xs-6 info-item-label">Årsmodell</div>
            <div className="col-xs-6 text-right info-item-text">{modelYear}</div>
        </div>
    </div>;
