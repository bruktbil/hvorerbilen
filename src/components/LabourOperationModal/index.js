import React, {Component} from "react";
import AddElementButton from "../../components/AddElementButton";
import CancelButton from "../../components/CancelButton";
import SaveButton from "../../components/SaveButton";
import Modal from "../../components/Modal";
import DeleteButton from "../../components/DeleteButton";
import DeleteWork from "../../components/DeleteWork";
import "./style.css";

export default class LabourOperationModal extends Component {
    static defaultState = {
        description: '',
        location: '',

        fromDay: '1',
        fromMonth: '0',
        fromYear: '2016',
        fromHour: '12',
        fromMinute: '0',
        toDay: '1',
        toMonth: '0',
        toYear: '2016',
        toHour: '12',
        toMinute: '0',
    };

    constructor(props) {
        super(props);
        this.state = props.editing ? props : LabourOperationModal.defaultState;
        this.deleteWorkModalId = "deleteWorkModalId" + props.index;

        this.onFieldChange = this.onFieldChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    onFieldChange(e) {
        if (e && e.target) {
            this.setState({
                ...this.state,
                [e.target.name]: e.target.value,
            });
        }
    }

    submitForm() {
        if (this.props.editing) {
            this.props.persistLabourClick(this.props.index, this.state);
        } else {
            this.props.persistLabourClick(this.state);
            this.setState(LabourOperationModal.defaultState);
        }
    }


    render() {
        const months = [
            "Januar", "Februar", "Mars", "April", "Mai", "Juni", "Juli",
            "August", "September", "Oktober", "November", "Desember"];

        const {description, location} = this.state;
        const currentYear = new Date().getFullYear();
        return (
            <div className="container-fluid">
                <div className="modal-body col-xs-12">

                    <div className="row">
                        <div className="col-xs-1 work-from-to">Fra</div>
                        <div className="form-group col-sm-2 col-xs-3">
                            <label className="control-label">Dag</label>
                            <select name="fromDay" className="form-control" value={this.state.fromDay}
                                    onChange={this.onFieldChange}>
                                <option disabled="disabled" value="">Dag</option>
                                {new Array(31).fill(1).map((value, index) =>
                                    <option key={index} value={1 + index}>{1 + index}</option>)}
                            </select>
                        </div>


                        <div className="form-group col-sm-3 col-xs-4">
                            <label className="control-label">Måned</label>
                            <select name="fromMonth" className="form-control" value={this.state.fromMonth}
                                    onChange={this.onFieldChange}>
                                <option default="default" disabled="disabled" value="">Måned</option>
                                {months.map((element, index) =>
                                    <option key={index} value={index}>{element}</option>)}
                            </select>
                        </div>

                        <div className="form-group col-sm-2 col-xs-3">
                            <label className="control-label">År</label>
                            <select name="fromYear" className="form-control" value={this.state.fromYear}
                                    onChange={this.onFieldChange}>
                                <option disabled="disabled" value="">År</option>
                                {new Array(10).fill(1).map((value, index) =>
                                    <option key={index} value={currentYear + index}>{currentYear + index}</option>)}
                            </select>
                        </div>

                        <div className="form-group col-sm-2 col-xs-5">
                            <label className="control-label">Time</label>
                            <select name="fromHour" className="form-control" value={this.state.fromHour}
                                    onChange={this.onFieldChange}>
                                <option disabled="disabled" value="">Time</option>
                                {new Array(24).fill(1).map((value, index) =>
                                    <option key={index} value={index}>{index}</option>)}
                            </select>
                        </div>

                        <div className="form-group col-sm-2 col-xs-5">
                            <label className="control-label">Minutt</label>
                            <select name="fromMinute" className="form-control" value={this.state.fromMinute}
                                    onChange={this.onFieldChange}>
                                <option disabled="disabled" value="">Minutt</option>
                                {new Array(4).fill(1).map((value, index) =>
                                    <option key={index} value={index * 15}>{index * 15}</option>)}
                            </select>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-xs-1 work-from-to">Til</div>
                        <div className="form-group col-sm-2 col-xs-3">
                            <label className="control-label">Dag</label>
                            <select name="toDay" className="form-control" value={this.state.toDay}
                                    onChange={this.onFieldChange}>
                                <option disabled="disabled" value="">Velg dag</option>
                                {new Array(31).fill(1).map((value, index) =>
                                    <option key={index} value={1 + index}>{1 + index}</option>)}
                            </select>
                        </div>


                        <div className="form-group col-sm-3 col-xs-4">
                            <label className="control-label">Måned</label>
                            <select name="toMonth" className="form-control" value={this.state.toMonth}
                                    onChange={this.onFieldChange}>
                                <option disabled="disabled" value="">Måned</option>
                                {months.map((element, index) =>
                                    <option key={index} value={index}>{element}</option>)}
                            </select>
                        </div>

                        <div className="form-group col-sm-2 col-xs-3">
                            <label className="control-label">År</label>
                            <select name="toYear" className="form-control" value={this.state.toYear}
                                    onChange={this.onFieldChange}>
                                <option disabled="disabled" value="">Måned</option>
                                {new Array(10).fill(1).map((value, index) =>
                                    <option key={index} value={currentYear + index}>{currentYear + index}</option>)}
                            </select>
                        </div>

                        <div className="form-group col-sm-2 col-xs-5">
                            <label className="control-label">Time</label>
                            <select name="toHour" className="form-control" value={this.state.toHour}
                                    onChange={this.onFieldChange}>
                                <option disabled="disabled" value="">Time</option>
                                {new Array(24).fill(1).map((value, index) =>
                                    <option key={index} value={index}>{index}</option>)}
                            </select>
                        </div>

                        <div className="form-group col-sm-2 col-xs-5">
                            <label className="control-label">Minutt</label>
                            <select name="toMinute" className="form-control" value={this.state.toMinute}
                                    onChange={this.onFieldChange}>
                                <option disabled="disabled" value="">Minutt</option>
                                {new Array(4).fill(1).map((value, index) =>
                                    <option key={index} value={index * 15}>{index * 15}</option>)}
                            </select>
                        </div>
                    </div>

                    <div className="form-group label-floating">
                        <label className="control-label" htmlFor="focusedInput1">Arbeid</label>
                        <input name="description" value={description} onChange={this.onFieldChange}
                               className="form-control" type="text"/>
                    </div>
                    <div className="form-group label-floating">
                        <label className="control-label" htmlFor="focusedInput1">Sted</label>
                        <input name="location" value={location} onChange={this.onFieldChange} className="form-control"
                               type="text"/>
                    </div>
                </div>

                <div className="modal-footer">
                    {this.props.editing &&
                    <span>
                        <DeleteButton modalId={this.deleteWorkModalId}/>
                        <Modal title="Er du sikker på at du vil slette arbeidet?" modalId={this.deleteWorkModalId}>
                        <DeleteWork deleteWorkItemClick={() => this.props.deleteWorkItemClick(this.props.index)}/>
                        </Modal>
                    </span>}
                    <CancelButton />
                    {this.props.editing ?
                        <SaveButton onClick={this.submitForm}/> :
                        <AddElementButton onClick={this.submitForm}/>}
                </div>
            </div>
        );
    }
}