import React from "react";
import "./style.css";
import Step from "../../enums/Step";

export default ({
    registrationNumber,
    completeStepClick,
    currentStepCompleted,
    changeStepClick,
    currentStepName,
    finishedInProcessClick,
    soldClick,
    acquisitionCarClick,
    isFinishedInProcess,
    isSold,
    isAcquisitionCar,
    startedInProcess
}) => {
    const stepStatusStyle = {color: isFinishedInProcess ? "gray" : currentStepCompleted ? "green" : "white"};
    const stepStateText =
        startedInProcess ?
            (currentStepCompleted ? "Steg fullført" : "Fullfør steg") :
            (currentStepCompleted ? "Prosess startet" : "Start prosess");
    const showCheckmark = startedInProcess ? "icon ion-checkmark-round drop-caret" : "";
    const chooseStepDisabled = {backgroundColor: currentStepCompleted && !isFinishedInProcess ? "rgba(0,103,171,0.2)" : "rgba(0,103,171,0)"};
    const chooseStepDisabledText = {color: currentStepCompleted && !isFinishedInProcess ? "rgba(255, 255, 255, 0.87)" : "#686868"};
    const chooseStepDisabledToggle = currentStepCompleted && !isFinishedInProcess ? "dropdown" : "";
    const interactiveElementStyle = !isFinishedInProcess ? "sub-nav-item interactive-item" : "sub-nav-item disabled-item";

    const finishStepDisabled = {backgroundColor: !isFinishedInProcess ? "rgba(0,103,171,0.2)" : "rgba(0,103,171,0)"};
    const finishStepDisabledText = {color: !isFinishedInProcess ? "rgba(255, 255, 255, 0.87)" : "#686868"};


    let registrationNumberSuffix = "";
    if (isSold) {
        registrationNumberSuffix = "Solgt";
    } else if (isAcquisitionCar) {
        registrationNumberSuffix = "Oppkjøperbil";
    }

    return (
        <div className="nav-container hidden-xs">
            <div className="navbar navbar-inverse custom-sub-nav">
                <div className="container-fluid col-md-12">
                    <a className="title-car-mobile visible-xs visible-sm col-xs-5">
                        {registrationNumber}
                    </a>

                    <div className="more-circle visible-xs visible-sm col-xs-1 col-sm-3 pull-right">
                        <a className="dropdown-toggle" data-toggle="dropdown">
                            <i className="icon ion-android-more-vertical more-icon"/>
                        </a>
                        <ul className="dropdown-menu more-dropdown">
                            <li onClick={finishedInProcessClick}>Ferdig i
                                prosess {isFinishedInProcess && "✔"}</li>
                            <li onClick={soldClick}>Solgt {isSold && "✔"}</li>
                            <li onClick={acquisitionCarClick}>Oppkjøperbil {isAcquisitionCar && "✔"}</li>
                        </ul>
                    </div>

                    <a className="label-car-mobile pull-right col-xs-4 col-sm-2 visible-xs visible-sm">
                        {registrationNumberSuffix}
                    </a>

                    <ul className="nav navbar-nav hidden-xs hidden-sm">
                        <li className="sub-nav-item">
                            <a className="title-car">
                                {registrationNumber}
                            </a>
                        </li>
                        <li className={interactiveElementStyle} style={chooseStepDisabled}>
                            <div className="btn-group">
                                <a href="#" data-target="#" data-toggle={chooseStepDisabledToggle}
                                   className="btn dropdown-toggle dropdown-button-status navbar-brand-white">
                                    <span style={chooseStepDisabledText}>Velg steg</span>
                                    <b className="caret drop-caret" style={chooseStepDisabledText}/>
                                </a>
                                <ul className="dropdown-menu subnav-dropdown">
                                    <li onClick={() => changeStepClick(Step.VALUATION.name)}>{Step.VALUATION.label}</li>
                                    <li onClick={() => changeStepClick(Step.RECEPTION.name)}>{Step.RECEPTION.label}</li>
                                    <li onClick={() => changeStepClick(Step.REPAIRS_WORKSHOP.name)}>{Step.REPAIRS_WORKSHOP.label}</li>
                                    <li onClick={() => changeStepClick(Step.TECHNICAL_WORKSHOP.name)}>{Step.TECHNICAL_WORKSHOP.label}</li>
                                    <li onClick={() => changeStepClick(Step.WASH_AND_SHINE.name)}>{Step.WASH_AND_SHINE.label}</li>
                                    <li onClick={() => changeStepClick(Step.PHOTO.name)}>{Step.PHOTO.label}</li>
                                    <li onClick={() => changeStepClick(Step.PUBLISHING.name)}>{Step.PUBLISHING.label}</li>
                                </ul>
                            </div>
                        </li>
                        <li className="sub-nav-item current-status">
                            <div className="form-group label-floating">
                                <label className="control-label status-label">Nåværende steg</label>
                                <p className="current-status-show">{currentStepName}</p>
                            </div>
                        </li>
                        <li className={interactiveElementStyle} style={finishStepDisabled}>
                            <a href="#" onClick={!isFinishedInProcess && completeStepClick}
                               className="navbar-brand-white complete-step"
                               style={finishStepDisabledText}>
                                {stepStateText}
                                <i className={showCheckmark} style={stepStatusStyle}/>
                            </a>
                        </li>
                        <li className="sub-nav-item-small">
                            <a className="label-car">
                                {registrationNumberSuffix}
                            </a>
                        </li>
                    </ul>
                    <ul className="nav navbar-nav navbar-right hidden-xs hidden-sm">
                        <li>
                            <div className="more-circle">
                                <a className="dropdown-toggle" data-toggle="dropdown">
                                    <i className="icon ion-android-more-vertical more-icon"/>
                                </a>
                                <ul className="dropdown-menu more-dropdown">
                                    <li onClick={finishedInProcessClick}>Ferdig i
                                        prosess {isFinishedInProcess && "✔"}</li>
                                    <li onClick={soldClick}>Solgt {isSold && "✔"}</li>
                                    <li onClick={acquisitionCarClick}>Oppkjøperbil {isAcquisitionCar && "✔"}</li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}
