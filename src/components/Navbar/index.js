import React, {Component} from "react";
import {Link} from "react-router";
import DrawerMenu from "../../components/DrawerMenu";
import "./style.css";

export default class openNav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: 0
        };

        this.toggleOpen = this.toggleOpen.bind(this);
    };

    toggleOpen() {
        this.setState({
            width: this.state.width === 250 ? 0 : 250
        });
    }

    render() {
        return (
            <div className="nav-container">
                <div className="navbar navbar-inverse custom-nav">
                    <div className="container-fluid toolbar-padding">
                        <span className="visible-xs hamburger-icon col-xs-1" onClick={this.toggleOpen}>
                            <i className="icon ion-navicon"/>
                        </span>
                        <DrawerMenu style={{width: this.state.width}} onClose={this.toggleOpen}/>
                        <ul className="nav navbar-nav hidden-xs">
                            <li className="nav-item">
                                <Link to="/">
                            <span className="navbar-brand-white">
                                Biler på lager
                            </span>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to="/new">
                            <span className="navbar-brand-white">
                                Registrer ny bil
                            </span>
                                </Link>
                            </li>
                        </ul>
                        <ul className="nav navbar-nav navbar-right hidden-xs">
                            <li>
                                <div className="nav-right-list-item">
                                    <div className="search-circle" data-toggle="tooltip" data-placement="bottom"
                                         title="Søkefunksjon kommer!">
                                        <i className="icon ion-android-search profile-icon"/>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div className="nav-right-list-item">
                                    <div className="search-circle" data-toggle="tooltip" data-placement="bottom"
                                         title="Profilvalg kommer!">
                                        <i className="icon ion-android-person profile-icon"/>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}