import React from "react";
import "./style.css";

export default ({title, children}) =>
    <div className="grey-box">
        <div className="box-title">{title}</div>
        <div className="box-content">{children}</div>
    </div>;