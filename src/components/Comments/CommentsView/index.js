import React from "react";
import Step from "../../../enums/Step";
import "./style.css";

export default ({category, comments}) =>
    <div>
        <div className="row">
            <div className="comment-category">{category === Step.NOT_STARTED.label ? "Annet" : category}</div>
        </div>
        <div className="row">
            {comments.map((comment, index) =>
                <div key={index} className="comment-text">{comment}</div>)}
            <p/>
        </div>
    </div>