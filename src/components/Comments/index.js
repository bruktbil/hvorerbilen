import React from "react";
import CommentsView from "./CommentsView";
import Step from "../../enums/Step";
import "./style.css";

const flatMapComments = comments => {
    const flatComments = {};
    comments.forEach(comment =>
        (flatComments[comment.category] = flatComments[comment.category] || []).push(comment.text));
    return flatComments;
};

export default ({comments = []}) => {
    const flatComments = flatMapComments(comments);
    const keys = Object.keys(flatComments);

    const firstHalfOfComments = keys.map((key, index) =>
    index < (keys.length / 2) && <CommentsView key={index} category={Step[key].label} comments={flatComments[key]}/>);

    const secondHalfOfComments = keys.map((key, index) =>
    index >= (keys.length / 2) && <CommentsView key={index} category={Step[key].label} comments={flatComments[key]}/>);

    return (
        <div className="comments-box">
            <div className="col-md-6 vertical-line">{firstHalfOfComments}</div>
            <div className="col-md-6 comments-second-col">{secondHalfOfComments}</div>
        </div>
    );
}

