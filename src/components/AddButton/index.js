import React from "react";
import "./style.css";
import PrimaryButton from "../PrimaryButton";


export default ({isFinishedInProcess, modalId}) => {
    const disabledButtonModal = isFinishedInProcess ? "" : "modal";

    return (
        <div className="add-button-box">
            <PrimaryButton isFinishedInProcess={isFinishedInProcess}
                           modalId={modalId}
                           buttonText="Legg til"
                           toggleData={disabledButtonModal}
                           iconClass="ion-plus-round"/>
        </div>
    )
};
