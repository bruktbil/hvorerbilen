import React from "react";
import "./style.css";


export default ({onClick, show, hide, name}) =>
    <div className="MainMenuItem" onClick={onClick}>
        <div className="container">
            <div className="row">
                <div className="col-xs-6 menu-item-style">{name}</div>
                <div className="col-xs-6 menu-item-style text-right">
                    <span className="glyphicon glyphicon-chevron-right"/>
                </div>
            </div>
        </div>
    </div>;