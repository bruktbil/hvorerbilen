import React from "react";
import PrimaryButton from "../PrimaryButton";

export default ({onClick, isFinishedInProcess}) =>
    <PrimaryButton isFinishedInProcess={isFinishedInProcess}
                   buttonText="Legg til"
                   iconClass="ion-plus-round"
                   dataDismiss="modal"
                   onClick={onClick}/>;
