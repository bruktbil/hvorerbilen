import React from "react";
import "./style.css";


export default ({modalId, isFinishedInProcess, buttonText, onClick, dataDismiss, toggleData, iconClass}) => {
    const disabledButtonColor = {backgroundColor: isFinishedInProcess ? "#999" : "#005c97"};
    const disabledButton = isFinishedInProcess ? "disabled" : "";
    const disabledButtonClass = isFinishedInProcess ? "btn disabled-button" : "btn add-button";

    return (
        <a href={"#" + modalId} data-toggle={toggleData} onClick={onClick} data-dismiss={dataDismiss}>
            <div className={disabledButtonClass} style={disabledButtonColor} disabled={disabledButton}>
                {buttonText}
                <i className={"icon icon-plus " + iconClass}/>
            </div>
        </a>)
};
