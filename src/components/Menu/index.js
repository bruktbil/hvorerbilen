import React from "react";
import MenuComponent from "../MenuComponent";

const menuComponents = [
    "Bilinfo",
    "Bruktbilinfo",
    "Bestilt arbeid",
    "Kommentarer",
    "Logg",
];

export default ({clickHandler, show}) =>
    <div className={"visible-xs" + (show ? "" : " hidden-xs")}>
        {menuComponents.map((component, index) =>
            <MenuComponent key={index} name={component} onClick={() => clickHandler(index)}/>)}
    </div>;