import React from "react";

export default () =>
    <div className="container-fluid visible-xs" style={{margin: "0 auto"}}>
        <div className="row">
            <div className="col-xs-12">
                <select style={{width: "100%"}}>
                    <option value="Takst">Takst</option>
                    <option value="Mottak">Mottak</option>
                    <option value="Verksted">Verksted</option>
                    <option value="Klargjøring">Klargjøring</option>
                    <option value="Publisert">Publisert</option>
                    <option value="Solgt">Solgt</option>
                </select>
            </div>
        </div>
    </div>;