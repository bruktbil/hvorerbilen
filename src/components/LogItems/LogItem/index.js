import React from "react";
import "./style.css";

export default ({item}) =>
    <div className="log-entry">
        <div className="log-heading">{item.type}</div>
        <div className="log-text">{item.type}</div>
        <div className="row log-metadata">
            <div className="col-xs-6">
                <div className="date-info">{item.date}</div>
            </div>
            <div className="col-xs-6 text-right">
                <div className="log-user">{item.userPerformingAction}</div>
            </div>
        </div>
    </div>