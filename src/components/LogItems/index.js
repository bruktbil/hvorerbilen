import React, {Component} from "react";
import LogItem from "./LogItem";
import CollapseIcon from "./CollapseIcon";
import "./style.css";

export default class LogItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: true
        }
    }

    toggleLogVisible() {
        this.setState({
            show: !this.state.show
        });
    }

    render() {
        return (
            <div className="log-box">
                <div className="log-title">
                    Logg
                    <CollapseIcon showExpanded={this.state.show} toggleClick={() => this.toggleLogVisible()}/>
                </div>
                <p className="log-coming-soon">
                    Loggen kommer her snart
                    <i className="icon ion-coffee coffee-icon"/>
                </p>
                {this.state.show ?
                    <div className="log-box-entries">
                        {this.props.logItems && this.props.logItems.map((logItem, index) =>
                            <LogItem key={index} item={logItem}/>)}
                    </div> :
                    <span />}
            </div>
        )
    }
}


