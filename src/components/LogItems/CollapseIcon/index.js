import React from "react";
import "./style.css";

export default ({toggleClick, showExpanded}) =>
    <div className="collapse-icon-box">
        <a href="#" onClick={() => toggleClick()}>
            {showExpanded ?
                <i className="icon ion-ios-arrow-forward icon-collapse"/> :
                <i className="icon ion-ios-arrow-back icon-collapse"/>}
        </a>
    </div>;
