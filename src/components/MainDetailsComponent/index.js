import React, {Component} from "react";
import "bootstrap-material-design/dist/css/bootstrap-material-design.css";
import "./style.css";
import ReadOnlyRow from "./ReadOnlyRow";
import EditButton from "./EditButton";

const paperStyle = {
    padding: "10px",
    margin: "10px 0",
    boxSizing: "border-box",
};

export default class MainDetailsComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {}
        }
    }

    componentDidMount() {
        const apiHost = 'http://localhost:3001/';
        fetch(apiHost + this.props.endpoint)
            .then(res => res.json())
            .then(json => this.setState({data: json}));
    }

    render() {
        const {name, show, editFunction} = this.props;
        const objects = this.state.data;

        return (
            <div className={"well " + (show ? "" : "hidden-xs")}>
                <div style={paperStyle}>
                    <h1 className="cardTitle">{name}</h1>{editFunction && <EditButton />}

                    {Object.keys(objects).map((label, index) =>
                        <ReadOnlyRow key={index} label={label} value={objects[label]}/>)}
                </div>
            </div>
        );
    }
}
