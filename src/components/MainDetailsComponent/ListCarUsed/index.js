import React from "react";
import "bootstrap-material-design/dist/css/bootstrap-material-design.css";
import "./style.css";

export default () =>
    <div className="row">
        <div className="col-xs-12 col-md-12" style={{marginTop: 10}}>
            
			<div className="car-info-list-item">
				<p className="car-info-item-name">BBnr</p>
				<p className="car-info-item text-right">33567</p>
			</div>

			<div className="car-info-list-item">
				<p className="car-info-item-name">Lokasjon</p>
				<p className="car-info-item text-right">BBSenter</p>
			</div>

			<div className="car-info-list-item">
				<p className="car-info-item-name">Kjøpt</p>
				<p className="car-info-item text-right">22.10.16</p>
			</div>

			<div className="car-info-list-item">
				<p className="car-info-item-name">Eier</p>
				<p className="car-info-item text-right">Minde</p>
			</div>

			<div className="car-info-list-item">
				<p className="car-info-item-name">Taksert av</p>
				<p className="car-info-item text-right">BBSenter</p>
			</div>

			<div className="car-info-list-item">
				<p className="car-info-item-name">Mtakst avd</p>
				<p className="car-info-item text-right">BBSenter</p>
			</div>
        </div>
    </div>;