import React from "react";
import "bootstrap-material-design/dist/css/bootstrap-material-design.css";
import "./style.css";

export default () =>
    <div className="row">
        <div className="col-xs-12 col-md-12" style={{marginTop: 10}}>
            
			<div className="car-info-list-item">
				<p className="car-info-item-name">Regnr</p>
				<p className="car-info-item text-right">SU33595</p>
			</div>

			<div className="car-info-list-item">
				<p className="car-info-item-name">Merke</p>
				<p className="car-info-item text-right">VW</p>
			</div>

			<div className="car-info-list-item">
				<p className="car-info-item-name">Modell</p>
				<p className="car-info-item text-right">Golf 110 DSG</p>
			</div>

			<div className="car-info-list-item">
				<p className="car-info-item-name">Farge</p>
				<p className="car-info-item text-right">Dyp sort</p>
			</div>

			<div className="car-info-list-item">
				<p className="car-info-item-name">Km</p>
				<p className="car-info-item text-right">21 750</p>
			</div>

			<div className="car-info-list-item">
				<p className="car-info-item-name">Årsmodell</p>
				<p className="car-info-item text-right">2015</p>
			</div>
        </div>
    </div>;