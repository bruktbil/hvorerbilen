import React from "react";

export default ({label, value}) =>
    <div>
        <div className="row">
            <div className="col-xs-6">{label}</div>
            <div className="col-xs-6 text-right">{value}</div>
        </div>
        <div className="row">
            <div className="col-xs-12">
                <hr/>
            </div>
        </div>
    </div>;
