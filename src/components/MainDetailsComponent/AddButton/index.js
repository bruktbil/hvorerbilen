import React from "react";
import "bootstrap-material-design/dist/css/bootstrap-material-design.css";
import "./style.css";

export default () =>
    <div className="row">
        <div className="col-xs-12 text-right" style={{marginTop: 10}}>
            
			<a href="javascript:void(0)" className="btn btn-primary btn-fab btn-fab-mini floating-button"><i className="material-icons">add</i></a>
        </div>
    </div>;