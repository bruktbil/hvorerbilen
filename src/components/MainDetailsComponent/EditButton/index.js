import React from "react";
import "bootstrap-material-design/dist/css/bootstrap-material-design.css";
import "./style.css";

export default () =>
    <div className="row">
        <div className="col-xs-12 text-right" style={{marginTop: 10}}>
            <a href="#"><i className="material-icons edit-button">edit</i></a>
        </div>
    </div>;