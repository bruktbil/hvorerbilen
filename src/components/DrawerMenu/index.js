import React from "react";
import {Link} from "react-router";
import "./style.css";

export default ({style, onClose}) =>
    <div id="drawerMenu" className="sidenav" style={style}>
        <a href="#" className="closebtn" onClick={onClose}>&times;</a>
        <div className="drawer-menu-items">
            <Link to="/" onClick={onClose}>Biler på lager</Link>
            <Link to="/new" onClick={onClose}>Registrer ny bil</Link>
        </div>
    </div>;