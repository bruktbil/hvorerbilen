import React from "react";
import "./style.css";

export default ({title, children}) =>
    <div className="grey-box-comments">
        <div className="box-title">{title}</div>
        <div className="box-content-comment">{children}</div>
	</div>;