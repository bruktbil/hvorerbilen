import React from "react";
import Modal from "../../Modal";
import EditIcon from "../../../components/EditIcon";
import LabourOperationModal from "../../../components/LabourOperationModal";
import "./style.css";

const editWorkModalId = "editWorkModal";

export default ({labour, isFinishedInProcess, index, updateWorkItemClick, deleteWorkItemClick}) =>
    <div className="row work-info-item">
        <div className="col-xs-1">
            <label>
                <input type="checkbox" className="work-checkbox"/>
            </label>
        </div>
        <div className="col-xs-8">
            <div className="row-content work-list-items">
                <h4 className="list-group-item-heading work-title">{labour.description}</h4>
                <p className="list-group-item-text work-text-location">{labour.location}</p>
                <p className="list-group-item-text work-text-date">
                    {`${labour.fromDay}.${labour.fromMonth}.${labour.fromYear} ${labour.fromHour}:${labour.fromMinute} `}
                    <i className="icon ion-ios-arrow-thin-right arrow-icon"/>
                    {`${labour.toDay}.${labour.toMonth}.${labour.toYear} ${labour.toHour}:${labour.toMinute}`}
                </p>
            </div>
        </div>
        <div className="col-xs-2 edit-work-icon">
            <EditIcon modalId={editWorkModalId + index} isFinishedInProcess={isFinishedInProcess}/>
            <Modal title="Rediger arbeid" modalId={editWorkModalId + index}>
                <LabourOperationModal {...labour}
                                      editing={true}
                                      index={index}
                                      persistLabourClick={updateWorkItemClick}
                                      deleteWorkItemClick={deleteWorkItemClick}/>
            </Modal>
        </div>
        <div className="half-line-separator col-xs-10 col-xs-offset-1"></div>
    </div>