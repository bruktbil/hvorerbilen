import React from "react";
import LabourItem from "./LabourItem";

export default ({labourOperations = [], isFinishedInProcess, updateWorkItemClick, deleteWorkItemClick}) =>
    <div className="container-fluid">
        <div className="row">
            <div className="list-group">
                {labourOperations.map((labourOperation, index) =>
                    <LabourItem key={index} index={index} labour={labourOperation}
                                isFinishedInProcess={isFinishedInProcess}
                                updateWorkItemClick={updateWorkItemClick}
                                deleteWorkItemClick={deleteWorkItemClick}/>)}
            </div>
        </div>
    </div>;
