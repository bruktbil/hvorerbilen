import React, {Component} from "react";
import AddElementButton from "../../components/AddElementButton";
import CancelButton from "../../components/CancelButton";
import Step from "../../enums/Step";
import "./style.css";

export default class AddComment extends Component {
    static defaultState = {
        category: '',
        text: ''
    };

    constructor(props) {
        super(props);
        this.state = AddComment.defaultState;

        this.submitForm = this.submitForm.bind(this);
        this.textChange = this.textChange.bind(this);
        this.optionChange = this.optionChange.bind(this);
    }

    textChange(e) {
        this.setState({text: e.target.value});
    }

    optionChange(e) {
        this.setState({category: e.target.value});
    }

    submitForm() {
        if (this.state.category && this.state.text) {
            this.props.addCommentClick(this.state.category, this.state.text);
            this.setState(AddComment.defaultState);
        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="modal-body">
                    <div className="form-group">
                        <label htmlFor="category" className="control-label">
                            <span>Kategori</span>
                        </label><br/>
                        <select id="category" className="form-control" onChange={this.optionChange}
                                value={this.state.category}>
                            <option default="default" disabled="disabled" value="">Velg steg</option>
                            <option value={Step.NOT_STARTED.name}>Annet</option>
                            <option value={Step.VALUATION.name}>{Step.VALUATION.label}</option>
                            <option value={Step.RECEPTION.name}>{Step.RECEPTION.label}</option>
                            <option value={Step.REPAIRS_WORKSHOP.name}>{Step.REPAIRS_WORKSHOP.label}</option>
                            <option value={Step.TECHNICAL_WORKSHOP.name}>{Step.TECHNICAL_WORKSHOP.label}</option>
                            <option value={Step.WASH_AND_SHINE.name}>{Step.WASH_AND_SHINE.label}</option>
                            <option value={Step.PHOTO.name}>{Step.PHOTO.label}</option>
                            <option value={Step.PUBLISHING.name}>{Step.PUBLISHING.label}</option>
                        </select>
                    </div>
                    <div className="form-group label-floating">
                        <label className="control-label" htmlFor="focusedInput1">Kommentar</label>
                        <input className="form-control" id="focusedInput1" type="text" value={this.state.text}
                               onChange={this.textChange}/>
                    </div>
                </div>
                <div className="modal-footer">
                    <CancelButton />
                    <AddElementButton onClick={this.submitForm}/>
                </div>
            </div>
        )
    };
}