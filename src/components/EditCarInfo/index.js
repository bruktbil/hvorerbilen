import React, {Component} from "react";
import In from "../ControlledInput";
import CancelButton from "../../components/CancelButton";
import SaveButton from "../../components/SaveButton";

export default class EditCarInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.onFieldChange = this.onFieldChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.registrationNumber) {
            const {registrationNumber, brand, model, color, mileage} = nextProps;
            this.setState({
                registrationNumber: registrationNumber,
                brand: brand,
                model: model,
                color: color,
                mileage: mileage
            });
        }
    }

    onFieldChange(e) {
        if (e && e.target) {
            this.setState({
                ...this.state,
                [e.target.name]: e.target.value,
            });
        }
    }

    render() {
        if (!this.state.registrationNumber) {
            return <div className="container-fluid modal-body">Feil: kunne ikke få data fra rotkomponenten.</div>;
        }

        const {registrationNumber, brand, model, color, mileage} = this.state;
        return (
            <div className="container-fluid">
                <div className="modal-body col-md-10">
                    <In onChange={this.onFieldChange} label="Registreringsnr" value={registrationNumber}
                        name="registrationNumber"/>
                    <In onChange={this.onFieldChange} label="Merke" value={brand} name="brand"/>
                    <In onChange={this.onFieldChange} label="Modell" value={model} name="model"/>
                    <In onChange={this.onFieldChange} label="Farge" value={color} name="color"/>
                    <In onChange={this.onFieldChange} label="Kilometerstand" value={mileage} name="mileage"/>
                </div>
                <div className="modal-footer">
                    <CancelButton />
                    <SaveButton onClick={() => this.props.editCarInfoClick(this.state)}/>
                </div>
            </div>
        );
    }
}