import React from "react";
import PrimaryButton from "../PrimaryButton";

export default ({onClick, isFinishedInProcess}) =>
    <PrimaryButton isFinishedInProcess={isFinishedInProcess}
                   buttonText="Bekreft"
                   iconClass="ion-checkmark-round"
                   dataDismiss="modal"
                   onClick={onClick}/>;