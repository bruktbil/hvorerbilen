export default {
    STATUS_CURRENT: "progress-bar-element-current",
    STATUS_DONE: "progress-bar-element-done",
    STATUS_NOT_CURRENT: "progress-bar-element-not-current",
    STATUS_DEFAULT: "progress-bar-element-not-current",
};
