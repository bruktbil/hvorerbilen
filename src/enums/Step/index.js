export default {
    "NOT_STARTED": {
        name: "NOT_STARTED",
        fieldName: "notStarted",
        label: "Inaktiv",
    },
    "VALUATION": {
        name: "VALUATION",
        fieldName: "valuation",
        label: "Takst",
    },
    "RECEPTION": {
        name: "RECEPTION",
        fieldName: "reception",
        label: "Mottak",
    },
    "REPAIRS_WORKSHOP": {
        name: "REPAIRS_WORKSHOP",
        fieldName: "repairsWorkshop",
        label: "Skadeverksted",
    },
    "TECHNICAL_WORKSHOP": {
        name: "TECHNICAL_WORKSHOP",
        fieldName: "technicalWorkshop",
        label: "Teknisk verksted",
    },
    "WASH_AND_SHINE": {
        name: "WASH_AND_SHINE",
        fieldName: "washAndShine",
        label: "Vask/Shine",
    },
    "PHOTO": {
        name: "PHOTO",
        fieldName: "photo",
        label: "Foto",
    },
    "PUBLISHING": {
        name: "PUBLISHING",
        fieldName: "publishing",
        label: "Publisering",
    }
};

export const getStepNameFromStepLabel = (steps, stepLabel) =>
    Object.keys(steps).filter(step => steps[step].label === stepLabel)[0];