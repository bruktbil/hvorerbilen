import {basePath} from "../../builders/BaseHostBuilder";

export const completeActiveStep = sessionId =>
    fetch(basePath + "/session/" + sessionId + "/states", {
        headers: new Headers({'Content-Type': 'application/json'}),
        method: "PUT"
    })
        .then(result => result.json())
        .catch(console.log);

export const setCurrentStep = (sessionId, sessionStepEnum) =>
    fetch(basePath + "/session/" + sessionId + "/states", {
        headers: new Headers({'Content-Type': 'application/json'}),
        method: "POST",
        body: sessionStepEnum
    })
        .then(result => result.json())
        .catch(console.log);

export const addComment = (sessionId, sessionStepEnum, commentText) =>
    fetch(basePath + "/session/" + sessionId + "/comments", {
        headers: new Headers({'Content-Type': 'application/json'}),
        method: "POST",
        body: JSON.stringify({
            category: sessionStepEnum,
            text: commentText
        })
    })
        .then(result => result.json())
        .catch(console.log);

export const addLabour = (sessionId, labourObject) =>
    fetch(basePath + "/session/" + sessionId + "/labouroperations", {
        headers: new Headers({'Content-Type': 'application/json'}),
        method: "POST",
        body: JSON.stringify(labourObject)
    })
        .then(result => result.json())
        .catch(console.log);

export const editCarInfo = (sessionId, carInfo) =>
    fetch(basePath + "/session/" + sessionId + "/carinfo", {
        headers: new Headers({'Content-Type': 'application/json'}),
        method: "PUT",
        body: JSON.stringify(carInfo)
    })
        .then(result => result.json())
        .catch(console.log);

export const editUsedCarInfo = (sessionId, usedCarInfo) =>
    fetch(basePath + "/session/" + sessionId + "/usedcarinfo", {
        headers: new Headers({'Content-Type': 'application/json'}),
        method: "PUT",
        body: JSON.stringify(usedCarInfo)
    })
        .then(result => result.json())
        .catch(console.log);