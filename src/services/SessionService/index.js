import {basePath} from "../../builders/BaseHostBuilder";

export const createSession = (carInfo, usedCarInfo) =>
    fetch(basePath + "/session", {
        headers: new Headers({'Content-Type': 'application/json',}),
        method: "POST",
        body: JSON.stringify({carInfo, usedCarInfo})
    })
        .then(res => res.json())
        .catch(console.log);

export const getSessionById = id =>
    fetch(basePath + "/session/" + id)
        .then(res => res.json())
        .catch(console.log);

export const getAllSessions = () =>
    fetch(basePath + "/session")
        .then(res => res.json())
        .catch(console.log);

const putValueToEndpointById = (sessionId, value, endpoint) =>
    fetch(basePath + "/session/" + sessionId + "/" + endpoint, {
        headers: new Headers({'Content-Type': 'application/json'}),
        method: "PUT",
        body: JSON.stringify(value)
    })
        .then(res => res.json())
        .catch(console.log);


export const setFinishedInProcess = (sessionId, value) =>
    putValueToEndpointById(sessionId, value, "finishedinprocess");

export const setSold = (sessionId, value) =>
    putValueToEndpointById(sessionId, value, "sold");

export const setAcquisitionCar = (sessionId, value) =>
    putValueToEndpointById(sessionId, value, "acquisitioncar");

export const updateWorkItem = (sessionId, workItemArrayIndex, workItemObject) =>
    putValueToEndpointById(sessionId, workItemObject, "labouroperations/" + workItemArrayIndex);

export const deleteWorkItem = (sessionId, workItemArrayIndex) =>
    fetch(basePath + "/session/" + sessionId + "/labouroperations/" + workItemArrayIndex, {
        headers: new Headers({'Content-Type': 'application/json'}),
        method: "DELETE"
    })
        .then(res => res.json())
        .catch(console.log);

