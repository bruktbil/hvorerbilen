# Hvor Er Bilen - frontend
This is the client application for the 'hvorerbilen' project, consisting of a full [React][react] development environment and server intended to serve data for the front-end application from port `:3000`.  
The various components which the development environment consists of are abstracted behind the [create-react-app][c-r-a] dependency, and can be accessed by running the ["eject" command][cra-eject]. 
## Prerequisites
The project is developed using Node.js v6.6.0, but should work well with all versions ranging from 4.0 and up.  
The latest Node.js version (bundled with npm) can be downloaded from the official [Node.js][node] homepage.

## Getting started
Follow these steps to get the application running:
- have the backend server running
- clone this repository
- open a terminal and navigate into the cloned folder
- run `npm i` to download the dependencies for the project
  - this is only required prior to the very first launch of the application
- run `npm start` to start the application

[react]: https://facebook.github.io/react/
[c-r-a]: https://github.com/facebookincubator/create-react-app
[cra-eject]: https://github.com/facebookincubator/create-react-app#converting-to-a-custom-setup
[node]: http://nodejs.org